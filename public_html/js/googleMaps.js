/* 
 * Markus Liechti
 * Resources: http://www.geocodezip.com/geoxml3_test/v3_FusionTables_query_sidebarF_local.html?country=Switzerland
 *            http://www.geocodezip.com/v3_FusionTablesLayer_worldmap_linkto.html
 */

function getLocationTypes() {

    var locations = [];

    var springs = {
        'items': [
            //{'title': 'Mägert BAUKU AG', 'lat': 46.5912901, 'lng': 7.6537595, 'info': '<div class="google-maps-infowindow"><b>Mägert Bauku AG</b><br/>Parallelstrasse 50<br/>3714 Frutigen</div>'},
            {'title': 'Mägert G&C Bautechnik AG', 'lat': -28.191704628349488, 'lng': 23.449196219444275, 'info': '<div class="google-maps-infowindow"><b>Mägert G&C Bautechnik AG</b><br/>Sonnenbergstrasse 11<br/>6052 Hergiswil</div>'},
            {'title': 'Mägert G&C 3', 'lat': -29.058807031427086, 'lng': 27.543727469444275, 'info': '<div class="google-maps-infowindow"><b>Mägert G&C Bautechnik AG</b><br/>Sonnenbergstrasse 11<br/>6052 Hergiswil</div>'},
            {'title': 'Mägert G&C 3', 'lat': -29.078807031427086, 'lng': 27.843727469444275, 'info': '<div class="google-maps-infowindow"><b>Mägert G&C Bautechnik AG</b><br/>Sonnenbergstrasse 11<br/>6052 Hergiswil</div>'}
        ],
        'icon': './images/google_maps_fountains.png',
        'markerClusterStyles':[
                    {
                        url: './images/test.png',
                        height: 25,
                        width: 25,
                        opt_anchor: [16, 0],
                        opt_textColor: '#FF00FF'
                    }
                ]
    };
    locations.push(springs);

    var roofs = {
        'items': [
            {'title': 'Roof 1', 'lat': -28.191704628349488, 'lng': 26.767067313194275, 'info': 'test 1'},
            {'title': 'Roof 2', 'lat': -28.867357297567626, 'lng': 25.888161063194275, 'info': 'test 2'},
            {'title': 'Roof 3', 'lat': -29.078807031427086, 'lng': 24.811500906944275, 'info': 'test 3'},
            {'title': 'Roof 4', 'lat': -31.71281688445632, 'lng': 23.449196219444275, 'info': 'test 4'},
            {'title': 'Roof 5', 'lat': -32.79057563632952, 'lng': 24.877418875694275, 'info': 'test 5'},
            {'title': 'Roof 6', 'lat': -31.56316043643865, 'lng': 27.843727469444275, 'info': 'test 6'}
        ],
        'icon': './images/google_maps_roofs.png',
        'markerClusterStyles':[
                    {
                        url: './images/roof_cluster.png',
                        height: 25,
                        width: 25,
                        opt_anchor: [16, 0],
                        opt_textColor: '#FF00FF'
                    }
                ]
    };
    locations.push(roofs);
    return locations;
}

var polygonStrockColor = '#7F7F7F';
var polygonStrokeWeight = 2;
var polygonStrokeOpacity = 0.5;
var polygonFillColor = '#D0D0D0';
var polygonFillOpacity = 0.5;

var markerCluster;
var markerClusters = [];
var markers = [];
var locationTypes;
var infos = [];

var mapCenterLat = document.getElementById("mapCenterLat");
var mapCenterLng = document.getElementById("mapCenterLng");
var mapZoom = document.getElementById("mapZoom");

function getInfoWindowOptions() {
    return {
        disableAutoPan: true,
        pixelOffset: new google.maps.Size(0, -14, 'px', 'px')
    };
}

function initialize() {

    locationTypes = getLocationTypes();

    var mapOptions = {
        center: new google.maps.LatLng(-28.53972416818728, 23.690895438194275),
        mapTypeId: 'roadmap',
        zoom: 6,
        scrollwheel: true
    };

    var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
    map.setTilt(45);
    //var locations = getLocations();


    for (var i = 0; i < locationTypes.length; i++) {

        var type = locationTypes[i];
        var locations = type.items;
        var icon = type.icon;
        // Contains all markers per type;
        var markerType = [];

        for (var j = 0; j < locations.length; j++) {
            var location = locations[j];
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(location.lat, location.lng),
                map: map,
                title: location.title,
                icon: icon
            });
            var content = location.info;
            var infowindow = new google.maps.InfoWindow();

            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {

                    /* close the previous info-window */
                    closeInfos();

                    infowindow.setContent(content);
                    infowindow.open(map, marker);

                    /* keep the handle, in order to close it on next click event */
                    infos[0] = infowindow;

                };
            })(marker, content, infowindow));


            //Contains all markers
            markers.push(marker);
            // contains all markers per type
            markerType.push(marker);
        }

        markerCluster = new MarkerClusterer(map, markerType, 
            {
                ignoreHidden: true, 
                styles:type.markerClusterStyles
            });
        markerClusters.push(markerCluster);
    }
    
    /*
     * Event listeners
     */
    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });

    google.maps.event.addListener(map, 'center_changed', function () {
        var latLng = map.getCenter();
        // show value on input
        showMapCenterValues(latLng);
    });
        
    google.maps.event.addListener(map, 'zoom_changed', function(){
        var zoom = map.getZoom();
        // show value on input
        showMapZoomValues(zoom);
    });
    
    /*
     * Show inital map values in controll
     */
    showMapZoomValues(map.getZoom());
    showMapCenterValues(map.getCenter());
}

/**
 * Hide all markers of passed type
 * 
 * @param {object} markerType
 * @returns void
 */
function hideMarkers(markerType) {
    var markerCluster = markerClusters[markerType];
    var markerss = markerCluster.getMarkers();
    for (var i = 0, length = markerss.length; i < length; i++) {
        markerss[i].setVisible(false);
    }
    markerCluster.repaint();
}

/**
 * Show all markers fo passed type
 * 
 * @param {object} markerType
 * @returns void
 */
function showMarkers(markerType) {
    var markerCluster = markerClusters[markerType];
    var markerss = markerCluster.getMarkers();
    for (var i = 0, length = markerss.length; i < length; i++) {
        markerss[i].setVisible(true);
    }
    markerCluster.repaint();
}

/**
 * Toogle visibility of type
 * 
 * @param {type} checkbox
 * @param {object} markerType
 * @returns {void}
 */
function toggleMarkers(checkbox, markerType){
    if(checkbox.checked){
        showMarkers(markerType);
    }else{
        hideMarkers(markerType);
    }
}

google.maps.event.addDomListener(window, 'load', initialize);


function closeInfos() {

    if (infos.length > 0) {

        /* detach the info-window from the marker ... undocumented in the API docs */
        infos[0].set("marker", null);

        /* and close it */
        infos[0].close();

        /* blank the array */
        infos.length = 0;
    }
}

/**
 * Show values on html
 */
function showMapCenterValues(latLng){
    mapCenterLat.value = latLng.lat();
    mapCenterLng.value = latLng.lng();
}
function showMapZoomValues(zoom){
    mapZoom.value = zoom;
}
